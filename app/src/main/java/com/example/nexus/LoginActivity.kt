package com.example.nexus

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.nexus.activities.MainActivity
import com.example.nexus.activities.RecoverActivity
import com.example.nexus.activities.RegistrationActivity
import com.example.nexus.fragments.ProfileFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.ktx.Firebase

class LoginActivity : AppCompatActivity() {

    private lateinit var emailInput : EditText
    private lateinit var passwordInput : EditText
    private lateinit var signIn : Button
    private lateinit var register : Button
    private lateinit var recover : Button
    private val auth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (auth.currentUser != null) {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }

        setContentView(R.layout.activity_login)

        init()

        onClickListeners()

    }

    private fun init() {
        emailInput = findViewById(R.id.emailInput)
        passwordInput = findViewById(R.id.passwordInput)
        signIn = findViewById(R.id.signInButton)
        register = findViewById(R.id.registerButton)
        recover = findViewById(R.id.recoverButton)
    }

    private fun onClickListeners() {
        signIn.setOnClickListener {

            val email = emailInput.text.toString()
            val password = passwordInput.text.toString()

            if (email.isEmpty() || password.isEmpty()) {
                Toast.makeText(this, "You should enter something!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            auth.signInWithEmailAndPassword(email, password).addOnCompleteListener { mission ->
                if (mission.isSuccessful) {
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                }else {
                    Toast.makeText(this,"Something went wrong!", Toast.LENGTH_SHORT).show()
                }
            }
        }
        register.setOnClickListener {
            startActivity(Intent(this, RegistrationActivity::class.java))
        }
        recover.setOnClickListener {
            startActivity(Intent(this, RecoverActivity::class.java))
        }
    }

}