package com.example.nexus.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.nexus.R
import com.example.nexus.models.UploadImage
import com.example.nexus.models.UserInfo

class RecyclerViewUserAdapter(private val postList: List<UserInfo>) : RecyclerView.Adapter<RecyclerViewUserAdapter.UserViewHolder>() {

    class UserViewHolder(itemViewUser: View) : RecyclerView.ViewHolder(itemViewUser) {

        private lateinit var Username: TextView

        init {
            Username = itemViewUser.findViewById(R.id.searchUsername)
        }

        fun setData(username: UserInfo) {

            Username.text = username.username

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val itemViewUser = LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        return UserViewHolder(itemViewUser)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.setData(postList[position])
    }

    override fun getItemCount(): Int {
        return postList.size
    }

}