package com.example.nexus.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.nexus.fragments.ProfileFragment
import com.example.nexus.viewpager.SavedPosts
import com.example.nexus.viewpager.UserPosts

class ViewPagerAdapter(activity: ProfileFragment) : FragmentStateAdapter(activity) {
    override fun getItemCount() = 2

    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> {
                return UserPosts()
            }
            else -> {
                return SavedPosts()
            }
        }
    }
}