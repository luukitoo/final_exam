package com.example.nexus.adapters

import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.nexus.R
import com.example.nexus.models.UploadImage
import com.google.firebase.database.FirebaseDatabase

class RecyclerViewPostAdapter(private val postList: List<UploadImage>) : RecyclerView.Adapter<RecyclerViewPostAdapter.PostViewHolder>() {

    class PostViewHolder(itemViewPost: View) : RecyclerView.ViewHolder(itemViewPost) {

        private lateinit var ProfileImage: ImageView
        private lateinit var Username: TextView
        private lateinit var PostImage: ImageView
        private lateinit var Description: TextView

        init {
            ProfileImage = itemViewPost.findViewById(R.id.profilePicture_post)
            Username = itemViewPost.findViewById(R.id.username_post)
            PostImage = itemViewPost.findViewById(R.id.postedPhoto)
            Description = itemViewPost.findViewById(R.id.description_post)
        }

        fun setData(uploadImage: UploadImage) {

            Glide.with(itemView).load(uploadImage.profileImageUrl).into(ProfileImage)
            Username.text = uploadImage.username
            Glide.with(itemView).load(uploadImage.postImageUrl).into(PostImage)
            Description.text = uploadImage.description

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val itemViewPost = LayoutInflater.from(parent.context).inflate(R.layout.item_post, parent, false)
        return PostViewHolder(itemViewPost)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.setData(postList[position])
    }

    override fun getItemCount(): Int {
        return postList.size
    }

}