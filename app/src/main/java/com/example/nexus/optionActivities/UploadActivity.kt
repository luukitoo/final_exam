package com.example.nexus.optionActivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.nexus.R
import com.example.nexus.activities.MainActivity
import com.example.nexus.models.UploadImage
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserInfo
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import java.util.logging.Logger.global

class UploadActivity : AppCompatActivity() {

    private lateinit var imageUrl_upload : EditText
    private lateinit var commit : EditText
    private lateinit var uploadImageButton : Button
    val auth = FirebaseAuth.getInstance()
    private val refUploaded = FirebaseDatabase.getInstance().getReference("UploadedImages")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload)

        imageUrl_upload = findViewById(R.id.imageUrl_upload)
        uploadImageButton = findViewById(R.id.uploadImageButton)
        commit = findViewById(R.id.description)

        uploadImageButton.setOnClickListener {

            val Username : String? = intent.getStringExtra("data1")
            val ProfilePhoto : String? = intent.getStringExtra("data2")

            val Url = imageUrl_upload.text.toString()
            val Description = commit.text.toString()
//            val ProfilePhoto = refImage.child(auth.currentUser?.uid!!).get().toString()

            if (Url.isEmpty() || Description.isEmpty()) {
                Toast.makeText(this, "Please enter image URL!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            } else {

                refUploaded.child(auth.currentUser?.uid!!)
                    .child(refUploaded.push().key ?: return@setOnClickListener)
                    .setValue(UploadImage( ProfilePhoto.toString(), Username.toString(), Url, Description))

                startActivity(Intent(this, MainActivity::class.java))

            }

        }

    }



}