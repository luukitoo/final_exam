package com.example.nexus.optionActivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.nexus.R
import com.example.nexus.activities.MainActivity
import com.example.nexus.models.ImagePicker
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase

class ImageActivity : AppCompatActivity() {

    private lateinit var imageUrl : EditText
    private lateinit var changeButton : Button
    val ref = FirebaseDatabase.getInstance().getReference("UserImages")
    val auth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)

        init()

        onClickListeners()

    }

    private fun init() {
        imageUrl = findViewById(R.id.imageUrl)
        changeButton = findViewById(R.id.changeImageButton)
    }

    private fun onClickListeners() {

        changeButton.setOnClickListener {

            val Url = imageUrl.text.toString()

            if (Url.isEmpty()) {
                Toast.makeText(this, "Please enter image URL!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }else {

                val imagePicker = ImagePicker(Url)
                ref.child(auth.currentUser?.uid!!).setValue(imagePicker)

                startActivity(Intent(this, MainActivity::class.java))


            }


        }

    }

}