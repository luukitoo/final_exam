package com.example.nexus.models

data class UploadImage(

    val profileImageUrl : String = "",
    val username : String = "",
    val postImageUrl : String = "",
    val description : String = ""

)
