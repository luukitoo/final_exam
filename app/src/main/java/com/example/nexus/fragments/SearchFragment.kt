package com.example.nexus.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nexus.R
import com.example.nexus.adapters.RecyclerViewPostAdapter
import com.example.nexus.adapters.RecyclerViewUserAdapter
import com.example.nexus.models.UploadImage
import com.example.nexus.models.UserInfo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import java.util.ArrayList

class SearchFragment : Fragment(R.layout.fragment_search) {

    private val refInfo = FirebaseDatabase.getInstance().getReference("UserInfo")
    private lateinit var userRecyclerview : RecyclerView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userRecyclerview = view.findViewById(R.id.RCView_posts)
        userRecyclerview.layoutManager = LinearLayoutManager(activity)
        userRecyclerview.setHasFixedSize(true)

        refInfo.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {

                val userArrayList = arrayListOf<UserInfo> ()

                for (userSnapshot in snapshot.children){

                    val user = userSnapshot.getValue(UserInfo::class.java)
                    userArrayList.add(user!!)

                }

                userRecyclerview.layoutManager = LinearLayoutManager(activity)
                userRecyclerview.adapter = RecyclerViewUserAdapter(userArrayList)

            }

            override fun onCancelled(error: DatabaseError) {
                return
            }

        })

    }

}