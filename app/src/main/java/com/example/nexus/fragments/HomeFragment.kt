package com.example.nexus.fragments

import android.os.Bundle
import android.provider.ContactsContract
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nexus.R
import com.example.nexus.adapters.RecyclerViewPostAdapter
import com.example.nexus.models.UploadImage
import com.example.nexus.models.UserInfo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import java.util.ArrayList

class HomeFragment : Fragment(R.layout.fragment_home) {

    private lateinit var RCView : RecyclerView
    private val refUploaded = FirebaseDatabase.getInstance().getReference("UploadedImages")
    private val auth = FirebaseAuth.getInstance()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        RCView = view.findViewById(R.id.RCView_posts)

        refUploaded.child(auth.currentUser?.uid!!).addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {

                val infoList = snapshot.children.toList()

                val postList = ArrayList<UploadImage> ()

                for (i in infoList){
                    postList.add(

                        i.getValue(UploadImage::class.java) ?:return

                    )

                }

                RCView.layoutManager = LinearLayoutManager(activity)
                RCView.adapter = RecyclerViewPostAdapter(postList)

            }

            override fun onCancelled(error: DatabaseError) {
                return
            }

        })

    }

}