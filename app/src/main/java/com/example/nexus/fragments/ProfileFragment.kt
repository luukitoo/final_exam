package com.example.nexus.fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.example.nexus.LoginActivity
import com.example.nexus.R
import com.example.nexus.activities.MainActivity
import com.example.nexus.adapters.ViewPagerAdapter
import com.example.nexus.models.ImagePicker
import com.example.nexus.models.UserInfo
import com.example.nexus.optionActivities.ImageActivity
import com.example.nexus.optionActivities.UploadActivity
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ProfileFragment : Fragment(R.layout.fragment_profile) {

    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager: ViewPager2
    private val refInfo = FirebaseDatabase.getInstance().getReference("UserInfo")
    private val refImage = FirebaseDatabase.getInstance().getReference("UserImages")
    private val auth = FirebaseAuth.getInstance()
    private val Uid = auth.currentUser?.uid!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val profilePicture = view.findViewById<ImageView>(R.id.profilePicture)
        tabLayout = view.findViewById(R.id.tabLayout)
        viewPager = view.findViewById(R.id.viewPager)
        val string = view.findViewById<TextView>(R.id.userName)
        val changeImageButton = view.findViewById<Button>(R.id.changeProfilePicture)
        val uploadImage = view.findViewById<Button>(R.id.uploadPicture)
        val logOut = view.findViewById<Button>(R.id.logOutButton)
        lateinit var intentURL : String


        changeImageButton.setOnClickListener {

            activity?.startActivity(Intent(activity, ImageActivity::class.java))

        }


        refImage.child(Uid).addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val imageUrl = snapshot.getValue(ImagePicker::class.java) ?:return
                Glide.with(this@ProfileFragment).load(imageUrl.imageUrl)
                    .placeholder(R.drawable.profile_default_lrg).into(profilePicture)
                intentURL = imageUrl.imageUrl

            }

            override fun onCancelled(error: DatabaseError) {
                return
            }

        })

        refInfo.child(Uid).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val username : UserInfo = dataSnapshot.getValue(UserInfo::class.java)  ?:return
                string.text = username.username
            }

            override fun onCancelled(error: DatabaseError) {
                return
            }
        })

        uploadImage.setOnClickListener {

            val intent = Intent(context, UploadActivity::class.java)
            intent.putExtra("data1", string.text.toString())
            intent.putExtra("data2", intentURL)
            startActivity(intent)

        }

        logOut.setOnClickListener {

            auth.signOut()
            startActivity(Intent(context, LoginActivity::class.java))
            activity?.finish()

        }

        viewPager.adapter = ViewPagerAdapter(this)
        TabLayoutMediator(tabLayout, viewPager) {tabLayout, pos ->

            when (pos) {
                0 -> {
                    tabLayout.setIcon(R.drawable.icons8_image_100)
                    tabLayout.text = "My Images"
                }
                1 -> {
                    tabLayout.setIcon(R.drawable.icons8_bookmark_256)
                    tabLayout.text = "Saved"
                }
            }

        }.attach()

    }

}