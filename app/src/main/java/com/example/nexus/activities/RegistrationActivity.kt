package com.example.nexus.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.nexus.LoginActivity
import com.example.nexus.R
import com.example.nexus.models.UserInfo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class RegistrationActivity : AppCompatActivity() {

    private lateinit var createUsername : EditText
    private lateinit var createEmail : EditText
    private lateinit var createPassword : EditText
    private lateinit var repeatPassword : EditText
    private lateinit var createAccount : Button
    private val auth = FirebaseAuth.getInstance()
    private val db = FirebaseDatabase.getInstance().getReference("UserInfo")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        init()

        onClickListeners()

    }

    private fun init() {
        createUsername = findViewById(R.id.createUsername)
        createEmail = findViewById(R.id.createEmail)
        createPassword = findViewById(R.id.createPassword)
        repeatPassword = findViewById(R.id.repeatPassword)
        createAccount = findViewById(R.id.createAccountButton)
    }

    private fun onClickListeners() {

        createAccount.setOnClickListener {

            val username = createUsername.text.toString()
            val email = createEmail.text.toString()
            val password = createPassword.text.toString()
            val repeatedPassword = repeatPassword.text.toString()

            if (username.isEmpty() || email.isEmpty() || password.isEmpty() || repeatedPassword.isEmpty()) {
                Toast.makeText(this, "You should enter something!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (password != repeatedPassword) {
                Toast.makeText(this, "Please try again!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { mission ->
                if (mission.isSuccessful) {

                    val userinfo = UserInfo(username)
                    db.child(auth.currentUser?.uid!!).setValue(userinfo)

                    startActivity(Intent(this, LoginActivity::class.java))
                    finish()
                }else {
                    Toast.makeText(this, "Something went wrong!", Toast.LENGTH_SHORT).show()
                }
            }

        }

    }

}
