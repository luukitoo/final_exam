package com.example.nexus.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.nexus.LoginActivity
import com.example.nexus.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.ktx.Firebase

class RecoverActivity : AppCompatActivity() {

    private lateinit var recoverPasswordEmail : EditText
    private lateinit var recoverButton: Button
    private val auth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recover)

        init()

        onClickListeners()

    }

    private fun init() {
        recoverPasswordEmail = findViewById(R.id.recoverPasswordEmail)
        recoverButton = findViewById(R.id.recoverPasswordButton)
    }

    private fun onClickListeners() {
        recoverButton.setOnClickListener {

            val email = recoverPasswordEmail.text.toString()

            if (email.isEmpty()) {
                Toast.makeText(this, "Please enter something!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            auth.sendPasswordResetEmail(email).addOnCompleteListener { mission ->
                if (mission.isSuccessful) {
                    startActivity(Intent(this, LoginActivity::class.java))
                    finish()
                }else {
                    Toast.makeText(this, "Please try again!", Toast.LENGTH_SHORT).show()
                }
            }

        }
    }

}