package com.example.nexus.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.nexus.R
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bottomNavView = findViewById<BottomNavigationView>(R.id.bottomNavMenu)
        val container = findNavController(R.id.fragmentContainer)

        val fragments = setOf<Int>(
            R.layout.fragment_home,
            R.layout.fragment_search,
            R.layout.fragment_profile
        )

        setupActionBarWithNavController(container, AppBarConfiguration(fragments))
        bottomNavView.setupWithNavController(container)
        
    }
}